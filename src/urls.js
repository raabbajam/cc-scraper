const main = 'cbssports.com';
const mainUri = `http://${main}/`;
const host = `ftggjs.football.${main}`;
const uri = `http://${host}/`;
const host2 = `auth.${main}`;
const uri2 = `https://${host2}/`;
const urls = {
  main,
  mainUri,
  uri,
  host,
  uri2,
  host2,
  login: `${uri2}login`,
  home: `${uri}standings/overall`,
  logout: `${uri}logout`,
};
module.exports = urls;
