const utils = {
  getLoginData,
  getFormData,
  setRefererHeader,
};
module.exports = utils;

function getLoginData(data = {}) {
  const {credentials, loginData} = data;
  const {username: userid, password} = credentials;
  const {loginDataForm} = loginData;
  const credentialsForm = {
    userid,
    password,
  };
  const form = Object.assign({}, loginDataForm, credentialsForm);
  return form;
}

function getFormData($, selector = 'form') {
  const form = {};
  const formArray = $(selector).serializeArray();
  formArray.forEach(({name, value}) => {
    form[name] = value;
  });
  return form;
}

function setRefererHeader(options = {}, referer = '') {
  options.headers = options.headers || {};
  options.headers.Referer = referer;
  return options;
}
