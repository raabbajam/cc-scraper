const Promise = require('bluebird');
const debug = require('debug')('cc:scraper:scraper');
const urls = require('./urls');
const utils = require('./utils');
const parser = require('./parser');
const scraper = {
  login,
  getStandings,
  logout,
};
module.exports = scraper;

function login(http, data = {}) {
  const {credentials} = data;
  const httpGetCookies = () => http.getCookies(urls.mainUri);
  if (credentials.cookieString) {
    debug(`Already has cookieString ${credentials.cookieString}. Checking session..`);
    http.setCookies(credentials.cookieString);
    return checkLoggedIn()
      .then((loggedIn) => {
        debug('checkLoggedIn', loggedIn);
        if (loggedIn) {
          debug('Session is OK');
          return http.getCookies();
        }
        debug('Session is expired.');
        return doLogin();
      });
  }
  debug('No cookie..');
  return doLogin();

  function checkLoggedIn() {
    return Promise.resolve()
      .then(() => {
        const options = {
          url: urls.home,
        };
        utils.setRefererHeader(options, urls.home);
        debug(options);
        return http.get(options)
          .get('body')
          .tap(http.saveHtml('checkLoggedIn'))
          .then(parser.checkLoggedIn);
      })
      .return(true)
      .catch((err) => {
        debug(err.message);
      })
      .catchReturn(false);
  }


  function doLogin() {
    debug('Go to logging in..');
    http.resetCookies();
    return Promise.resolve()
      .then(() => {
        const options = {
          url: urls.home,
        };
        utils.setRefererHeader(options, urls.home);
        debug(options);
        return http.get(options)
          .get('body')
          .tap(http.saveHtml('login'))
          .then(parser.getLoginData);
      })
      .then((loginData) => {
        const form = utils.getLoginData({credentials, loginData});
        const options = {
          url: urls.login,
          headers: {
            Host: urls.host2,
          },
          form,
        };
        utils.setRefererHeader(options, urls.login);
        debug('postLogin', options);
        return http.post(options)
          .tap((response) => debug(response.headers))
          .get('body')
          .tap(http.saveHtml('postLogin'))
          .then(parser.postLogin);
      })
      .then(httpGetCookies)
      .then((cookies) => {
        debug('Got new cookies', cookies);
        return cookies;
      })
      .tap(http.saveJson('login'))
      .catch((error) => {
        debug(error.message);
        throw error;
      });
  }
}

function getStandings(http) {
  return Promise.resolve()
    .then(() => {
      const options = {
        url: urls.home,
      };
      return http.get(options)
        .get('body')
        .tap(http.saveHtml('standings'))
        .then(parser.getStandings);
    });
}

function logout(http) {
  debug('logout');
  return Promise.resolve()
    .then(() => {
      const options = {
        url: urls.logout,
      };
      debug(options);
      return http.get(options);
    })
    .tap(http.saveHtml('logout'))
    .return(true);
}
