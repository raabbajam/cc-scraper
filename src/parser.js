const cheerio = require('cheerio');
const utils = require('./utils');
const parser = {
  getLoginData,
  checkLoggedIn,
  postLogin,
  getStandings,
};
module.exports = parser;

function getLoginData(html) {
  const $ = cheerio.load(html);
  const loginDataForm = utils.getFormData($, '#login_form');
  const loginData = {loginDataForm};
  return loginData;
}

function checkLoggedIn(html) {
  const $ = cheerio.load(html);
  const title = $('title').text().trim();
  const failFlag = /sign in/i.test(title);
  if (failFlag) {
    const error = 'Session expired!';
    throw new Error(error);
  }
  return true;
}

function postLogin(html) {
  const $ = cheerio.load(html);
  const title = $('title').text().trim();
  const failFlag = /sign in/i.test(title);
  if (failFlag) {
    const error = 'Session expired!';
    throw new Error(error);
  }
  return true;
}

function getStandings(html) {
  const $ = cheerio.load(html);
  const $rows = $('.data tr[id]').get();
  const standings = $rows.map(($row) => {
    const [
      name,
      win,
      loss,
      tie,
      pct,
      gb,
      streak,
      wks,
      pf,
      back,
      pa,
    ] = $('td', $row).get().map(getString);
    const standing = {
      name,
      win,
      loss,
      tie,
      pct,
      gb,
      streak,
      wks,
      pf,
      back,
      pa,
    };
    return standing;

    function getString($td) {
      let text = $($td).text().trim();
      text = /^[\d\.]+$/.test(text) ? Number(text) : text;
      return text;
    }
  });
  return standings;
}
