const Promise = require('bluebird');
const Http = require('http-scraper');
const assert = require('assert');
const urls = require('./urls');
const scraper = require('./scraper');
const debug = require('debug')('cc:scraper:main');
function Scraper(options = {}) {
  const type = 'cbs';
  const {uri, host} = urls;
  const followAllRedirects = true;
  const defaults = {uri, host, type, followAllRedirects};
  const settings = Object.assign({}, defaults, options);
  const {capture, capturePath} = settings;
  const httpSettings = {capture, capturePath, uri, host, type, followAllRedirects};
  const http = Http(httpSettings);
  const {credentials} = settings;
  const {username, password} = credentials;
  assert.ok(username, 'credentials username not found');
  assert.ok(password, 'credentials password not found');
  const properties = {settings, http, credentialsChangedHandlers: []};
  const prototypes = {
    login,
    checkCredentials,
    getStandings,
    logout,
    onCredentialsChange,
    handleCredentialsChanged,
  };
  return Object.assign(Object.create(prototypes), properties);
}
module.exports = Scraper;

function login() {
  const {credentials} = this.settings;
  return Promise.resolve()
    .then(() => scraper.login(this.http, {credentials}))
    .tap((cookies) => this.handleCredentialsChanged({cookies}));
}

function checkCredentials() {
  return Promise.resolve()
    .then(() => this.login())
    .then(() => this.logout());
}

function getStandings() {
  return Promise.resolve()
    .then(() => this.login())
    .then(() => scraper.getStandings(this.http));
}

function logout() {
  return Promise.resolve()
    .then(() => scraper.logout(this.http));
}
// helper functions
function onCredentialsChange(handler) {
  this.credentialsChangedHandlers = this.credentialsChangedHandlers.concat(handler);
}

function handleCredentialsChanged(credentials = {}) {
  const {cookies = []} = credentials;
  if (cookies) {
    const newCookie = cookies.find(({key}) => key === 'pid').cookieString();
    const oldCookie = this.settings.credentials.cookieString || '';
    if (newCookie === oldCookie) {
      debug('Cookie not changed!!');
      return Promise.resolve();
    }
    debug('Cookie changed!!');
    this.settings.credentials.cookieString = newCookie;
    credentials.cookies = newCookie;
  }
  return Promise.map(this.credentialsChangedHandlers, (handler) =>
    handler(credentials));
}
