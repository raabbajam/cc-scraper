const debug = require('debug')('cc:scraper:scraper:test:utils');
const {resolve} = require('path');
const {ensureFileSync, writeFile, readFileSync} = require('fs-extra');
const utils = {
  getTestData,
};
module.exports = utils;

const integration = (() => {
  const jsonFiles = [
  ];
  const texts = [
    'getCookieString',
  ];
  const htmls = [
    'getStandings',
  ];
  const methods = {};
  jsonFiles.forEach((file) => {
    methods[file] = () => JSON.parse(readFileSync(getFilePath(file, 'queries', 'json'), 'utf8'));
  });
  texts.forEach((file) => {
    methods[file] = () => readFileSync(getFilePath(file, 'txt', 'txt'), 'utf8');
  });
  htmls.forEach((file) => {
    methods[file] = () => readFileSync(getFilePath(file, 'html', 'html'), 'utf8');
  });
  methods.saveCredentials = (credentials = {}) => {
    // @TO DO use library to make sure file is created
    // @TO DO ignore cookie file from git
    debug(credentials);
    const {cookies} = credentials;
    if (cookies) {
      const file = 'getCookieString';
      writeFile(getFilePath(file, 'txt', 'txt'), cookies, 'utf8', (err) => {
        if (err) {
          return Promise.reject(err);
        }

        return Promise.resolve();
      });
    }
  };

  return methods;
})();

const testModules = {
  // helper,
  integration,
  // parser,
  // validator,
};
function getTestData(testModule) {
  return testModules[testModule];
}

function getFilePath(filename, subpath = '', extension = '') {
  const filepath = resolve(__dirname, subpath, `${filename}.${extension}`);
  ensureFileSync(filepath);
  return filepath;
}
