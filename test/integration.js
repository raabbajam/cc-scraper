/* eslint no-console: 0 */
const test = require('tape');
const path = require('path');
const Scraper = require('../src');
const parser = require('../src/parser');
const {getTestData} = require('./test-utils');
const testData = getTestData('integration');
const timeout = 240000;
const credentials = {
  username: process.env.CC_USERNAME,
  password: process.env.CC_PASSWORD,
  cookieString: testData.getCookieString(),
};
const userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML,' +
  ' like Gecko) Chrome/51.0.2704.19 Safari/537.36';
const capturePath = path.resolve(__dirname, 'tmp');
const options = {credentials, userAgent, capture: true, capturePath};
const scraper = Scraper(options);
scraper.onCredentialsChange(testData.saveCredentials);
test('scraper login', {timeout}, (assert) => {
  assert.plan(1);
  scraper.login()
    .then((cookies) => {
      // console.log(JSON.stringify(cookies, null, 2));
      assert.ok(cookies.length > 0, 'should return array of cookies');
    })
    .catch(assert.end);
});
test('scraper checkCredentials', {timeout}, (assert) => {
  assert.plan(1);
  scraper.checkCredentials()
    .then((ok) => {
      // console.log(JSON.stringify(ok, null, 2));
      assert.ok(ok, 'should ok');
    })
    .catch(assert.end);
});
test('parser getStandings', {timeout}, (assert) => {
  assert.plan(1);
  const html = testData.getStandings();
  const standings = parser.getStandings(html);
  // console.log(JSON.stringify(standings, null, 2));
  assert.ok(Boolean(standings), 'should ok');
});
test('scraper getStandings', {timeout}, (assert) => {
  assert.plan(1);
  scraper.getStandings()
    .then((standings) => {
      // console.log(JSON.stringify(standings, null, 2));
      assert.ok(Boolean(standings), 'should ok');
    })
    .catch(assert.end);
});
