# auction-scraper

[![NPM version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Coveralls Status][coveralls-image]][coveralls-url]
[![Dependency Status][depstat-image]][depstat-url]
[![Downloads][download-badge]][npm-url]

> auction-scraper

## Install

```sh
npm i -D auction-scraper
```

## Usage

```js
import auctionScraper from "auction-scraper"

auctionScraper() // true
```

## License

MIT © [Raabb Ajam](https://github.com/raabbajam)

[npm-url]: https://npmjs.org/package/auction-scraper
[npm-image]: https://img.shields.io/npm/v/auction-scraper.svg?style=flat-square

[travis-url]: https://travis-ci.org/raabbajam/auction-scraper
[travis-image]: https://img.shields.io/travis/raabbajam/auction-scraper.svg?style=flat-square

[coveralls-url]: https://coveralls.io/r/raabbajam/auction-scraper
[coveralls-image]: https://img.shields.io/coveralls/raabbajam/auction-scraper.svg?style=flat-square

[depstat-url]: https://david-dm.org/raabbajam/auction-scraper
[depstat-image]: https://david-dm.org/raabbajam/auction-scraper.svg?style=flat-square

[download-badge]: http://img.shields.io/npm/dm/auction-scraper.svg?style=flat-square
